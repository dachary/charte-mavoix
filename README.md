# Notre charte

## #MaVoix est …

Encore en cours de rédaction collective ;-). Version à date.

**Une contribution à la démocratie** : nous constatons les limites et l’échec de la démocratie représentative et nous souhaitons expérimenter d’autres méthodes qui tendent à poursuivre l’idéal démocratique.

**En mouvement** : ni parti ni association, #MaVoix c’est des femmes et des hommes d’horizons divers souhaitant expérimenter une méthode démocratique pour faire rentrer les citoyens à l’Assemblée Nationale en juin 2017. Nous ne nous réclamons d’aucun parti ni d’aucune idéologie. 
    
**Un timing et un lieu** : les législatives de juin 2017 et l’Assemblée Nationale / Palais Bourbon.

**Un pari sur l’intelligence collective et fondé sur la confiance** : les membres de #MaVoix sont riches de leurs expériences et parcours individuels. Nous n’avons pas de programme et pensons que c’est de ces interactions que naissent les meilleures décisions. Une assemblée ouverte : tous les citoyens sont les bienvenus pour participer au mouvement, aux discussions, aux débats. La méthode est par définition neutre, chaque contributeur est porteur de convictions personnelles. Un respect: chaque membre de #MaVoix est doté d’une voix pour se faire entendre. Une voix égale une autre voix.

**En réseau** : en ligne, hors ligne, local et national, de manière horizontale et coopérative, en apportant méthodes et soutien pour tous les citoyens désireux de s’impliquer ou d’organiser des événements démocratiques. Les discussions & les décisions de #MaVoix sont publiques, réalisées autant que possible via des canaux accessibles à tous, en utilisant des outils et/ou logiciels libres et ouverts à tous.

- en mouvement et en réseau, horizontal, coopératif
- une voix égale une voix
- expérimentation démocratique pour les législatives de juin 2017
- pas de financement public
- candidats volontaires formés et tirés au sort
- les députés relaieront les décisions de leurs électeurs pendant 5 ans
- Obligation morale des candidats à siéger 100% du temps législatif
